package com.dyts_rs_filesuploader;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.dyts_rs_filesuploader.webservice.APIs;
import com.dyts_rs_filesuploader.webservice.JSONCallback;
import com.dyts_rs_filesuploader.webservice.JSONCallbackMultipart;
import com.dyts_rs_filesuploader.webservice.Retrofit;
import com.google.gson.Gson;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class MyJobService extends JobService {
    private static final String TAG = MyJobService.class.getSimpleName();
    private Context context;
    private File[] eventFiles;
    private int eventLooper = 0;
    private String eventMainFolder = "";
    String NOTIFICATION_CHANNEL_ID = "Channel2Id_dyts_rs";
    private int notificationID = 2;
    String langCode = "EN";
    private Violation violation;
    String jsonString, data = "Event scheduler called";
    int eventUploadStatus = 0;
    @Override
    public void onCreate() {
        super.onCreate();
        context = MyJobService.this;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d("JobService","onStartJob");
        //sendBroadcast(new Intent(this, JobSchedulerReceiver.class));

        eventMainFolder = Environment.getExternalStorageDirectory() + String.format(context.getString(R.string.hidden_storage_folder), "DyTS");
        if (isNetworkConnected(context)) {
            startForeground(notificationID,serviceRunningNotification("DyTS Events Uploader is running."));
            if (getEventFolder()) {
                // API_UploadEventDetail(eventFiles[eventLooper]);
                getFileData(eventFiles[eventLooper]);
            }
        } else {
            startForeground(notificationID,serviceRunningNotification("No internet connection."));
        }
        jobFinished(params,true);
        return true;
    }
    private void startJobScheduler()
    {
        ComponentName serviceComponent = new ComponentName(this, MyJobService.class);
        JobInfo.Builder jobInfo = new JobInfo.Builder(1001, serviceComponent);
        jobInfo.setRequiresCharging(true);
        jobInfo.setRequiresDeviceIdle(false);
        jobInfo.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            jobInfo.setPeriodic(60000 * 5);
        } else {
//            jobInfo.setPeriodic(60000 * 15,60000 * 15);
//            jobInfo.setMinimumLatency(60000 * 15);
            jobInfo.setPeriodic(15 * 60 * 1000, 5 * 60 *1000);
        }
        jobInfo.setPersisted(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            jobInfo.setRequiresBatteryNotLow(true);
        }
        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(jobInfo.build());

    }
    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    private boolean getEventFolder() {
        boolean isExistEvent = false;
        try {
            File fileDir = new File(eventMainFolder);
            if (fileDir.exists()) {
                File fileDirChild = new File(fileDir, context.getString(R.string.events_folder));
                if (fileDirChild.exists()) {
                    eventFiles = fileDirChild.listFiles();
                    for (int i = 0; i < eventFiles.length; i++) {
                        Log.d("FileDir", "dir=" + eventFiles[i].getName());
                    }
                    if (eventFiles != null) {
                        if (eventFiles.length > 0) {
                            isExistEvent = true;
                            Log.d("FileDir", "Events Length=" + eventFiles.length);
                        } else {
                            startForeground(notificationID,serviceRunningNotification("Events not found."));
                        }
                    }
                } else {
                    Log.d("FileDir", "Event Folder not exist.");
                    startForeground(notificationID,serviceRunningNotification("Event Folder not exist."));
                }
            } else {
                Log.d("FileDir", "Event Main Folder not exist.");
                startForeground(notificationID,serviceRunningNotification("Event Main Folder not exist."));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isExistEvent;
    }

    private void getFileData(File file) {
        Log.d("FileDir", "" + file.getName());
        try {

            File uploadFile = new File(file, "UploadFile.txt");
            if (uploadFile.getAbsoluteFile().exists()) {
                //Read text from file
                StringBuilder text = new StringBuilder();


                BufferedReader br = new BufferedReader(new FileReader(uploadFile));
                String line;
                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                br.close();
                jsonString = text.toString();
                Logger.e("jsonString", jsonString);

                if (!jsonString.isEmpty()) {
                    violation = new Gson().fromJson(jsonString, Violation.class);
                    SharedPreferences pref = context.getSharedPreferences(APIs.PREF_NAME, 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString(APIs.AUTH_TOKEN, violation.getAuthorization());
                    editor.apply();
                    String langCode = violation.getLanguageCode();
                    if (langCode == null) {
                        langCode = "EN";
                    }
                    sendEventInfoApi(file);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void sendEventFinalInfoApi() {
       /*/ try {
            HashMap<String, String> infoParams = new HashMap<>();
            infoParams.put("userId", violation.getUserId());
            infoParams.put("status", "" + eventUploadStatus);
            infoParams.put("jsonData", jsonString);
            infoParams.put("stamps", violation.getViolationDateTime());
            infoParams.put("data", data);

            Retrofit.with(this)
                    .setAPI(APIs.API_EVENT_DETAILS)
                    .setParameters(infoParams, langCode)
                    .setCallBackListener(new JSONCallback(this) {
                        @Override
                        protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {

                        }

                        @Override
                        protected void onFailed(int statusCode, String message) {
                            Logger.e(message);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }
    private void sendEventInfoApi(final File file) {
        API_UploadEventDetail(file);
        /*try {
            HashMap<String, String> infoParams = new HashMap<>();
            infoParams.put("userId", violation.getUserId());
            infoParams.put("status", "" + eventUploadStatus);
            infoParams.put("jsonData", jsonString);
            infoParams.put("stamps", violation.getViolationDateTime());
            infoParams.put("data", data);

            Retrofit.with(this)
                    .setAPI(APIs.API_EVENT_DETAILS)
                    .setParameters(infoParams, langCode)
                    .setCallBackListener(new JSONCallback(this) {
                        @Override
                        protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {
                            API_UploadEventDetail(file);
                        }

                        @Override
                        protected void onFailed(int statusCode, String message) {
                            Logger.e(message);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void API_UploadEventDetail(final File file) {
        try {

            HashMap<String, String> params = new HashMap<>();
            params.put("userId", violation.getUserId());
            params.put("vehicleNo", violation.getVehicleNo());
            params.put("reasonId", String.valueOf(violation.getReasonId()));
            params.put("violationDateTime", violation.getViolationDateTime());
            params.put("violationLatitude", String.valueOf(violation.getViolationLatitude()));
            params.put("violationLongitude", String.valueOf(violation.getViolationLongitude()));

            HashMap<FileModel, File> fileParams = new HashMap<>();
            FileModel mFileModel;

            // Set Captured Image
            String imagePath = violation.getFiles().get(0).getPath();
            File mFile = new File(imagePath);
            if (mFile.exists()) {
                Log.e("Image File Exist: ", imagePath);
                mFileModel = new FileModel();
                mFileModel.setName(mFile.getName());
                mFileModel.setKey("violationImage");
                mFileModel.setPath(imagePath);
                mFileModel.setType(FileModel.MediaType.MEDIA_TYPE_IMAGE);
                fileParams.put(mFileModel, mFile.getAbsoluteFile());
            } else {
                Log.e("Image File Not Exist: ", imagePath);
            }

            // Set Captured Video
            String videoPath = violation.getFiles().get(1).getPath();
            mFile = new File(videoPath);
            if (mFile.exists()) {
                Log.e("Video File Exist: ", videoPath);
                mFileModel = new FileModel();
                mFileModel.setName(mFile.getName());
                mFileModel.setKey("violationVideo");
                mFileModel.setPath(videoPath);
                mFileModel.setType(FileModel.MediaType.MEDIA_TYPE_VIDEO);
                fileParams.put(mFileModel, mFile.getAbsoluteFile());
            } else {
                Log.e("Video File Not Exist: ", videoPath);
            }

            // Set Number Plate Image
            String numberPlateImagePath = violation.getFiles().get(2).getPath();
            mFile = new File(numberPlateImagePath);
            if (mFile.exists()) {
                Log.e("ImageFileExist:", numberPlateImagePath);
                mFileModel = new FileModel();
                mFileModel.setName(mFile.getName());
                mFileModel.setKey("violationNumberPlateImage");
                mFileModel.setPath(numberPlateImagePath);
                mFileModel.setType(FileModel.MediaType.MEDIA_TYPE_IMAGE);
                fileParams.put(mFileModel, mFile.getAbsoluteFile());
            } else {
                Log.e("ImageFileNotExist:", numberPlateImagePath);
            }
            Log.e("Error", String.valueOf(fileParams.size()));

            Retrofit.with(context)
                    .setFileParameters(params, fileParams, langCode)
                    .setAPI(APIs.API_UPLOAD_EVENT)
                    .setCallBackListenerMultipart(new JSONCallbackMultipart(context) {

                        @Override
                        protected void onSuccess(int statusCode, JSONObject jsonObject, String tag) {
                            Log.e("onSuccess:", "onSuccess");
                            try {
                                if (jsonObject.getBoolean("success")) {
                                    Log.e("eventFiles.length:", "eventFiles.length" + eventFiles.length);
                                    if (eventLooper == eventFiles.length - 1) {
                                        eventLooper = 0;

                                        eventUploadStatus = 1;
                                        sendEventFinalInfoApi();

                                        startForeground(notificationID,serviceRunningNotification("All Events successfully uploaded."));
                                        try {
                                            for (File eventFile : eventFiles) {
                                                try {
                                                    Log.e("Delete directory:", "=>" + eventFile.getName());
                                                    //eventFile.delete();
                                                    deleteEventData(eventFile);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    } else {
                                        if (eventLooper < eventFiles.length - 1) {
                                            eventLooper++;
                                            sendEventInfoApi(eventFiles[eventLooper]);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        protected void onFailed(int statusCode, String message) {
                            Log.e("Error:", message);
                            eventUploadStatus = 2;
                            sendEventInfoApi(file);
                        }
                    }, String.valueOf("Event"));
        } catch (Exception e) {
            e.printStackTrace();
        }
/*    } catch(
    Exception e)

    {
        e.printStackTrace();
        serviceRunningNotification("Exception Violation " + e.getMessage());
    }

}

                }catch(Exception e){
                        //You'll need to add proper error handling here
                        e.printStackTrace();
                        }

                        }else{
                        serviceRunningNotification(file.getName()+"Dir UploadFile.txt not found.");
                        if(eventLooper<eventFiles.length-1){
        eventLooper++;
        API_UploadEventDetail(eventFiles[eventLooper]);
        }
        }
        }catch(Exception e){
        e.printStackTrace();
        serviceRunningNotification(file.getName()+" Dir Exception UploadFile.txt not found.");
        }*/
    }

    private void deleteEventData(File file) {
        try {
            Log.e("Delete Event Dir:", "=>" + file.getName());
            FileUtils.deleteDirectory(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Notification serviceRunningNotification(String title) {
        try {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel(notificationManager);
            }

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            Intent notificationIntent = new Intent(context, MainActivity.class);
            // set intent so it does not start a new activity
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            notificationBuilder.setContentIntent(contentIntent);
            int icon = R.mipmap.ic_launcher;
            //notificationBuilder.setColor(getResources().getColor(R.color.black));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                icon = R.mipmap.ic_launcher;
                notificationBuilder.setColor(getResources().getColor(R.color.colorPrimary));
            }
            notificationBuilder.setAutoCancel(true)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(icon)
                    .setVibrate(null)
                    .setSound(null)
                    .setTicker("DyTS events service is running")
                    .setContentTitle("Job Service is running")
                    .setContentText("" + title);

            assert notificationManager != null;
            notificationManager.notify(notificationID, notificationBuilder.build());
            return notificationBuilder.build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void createNotificationChannel(NotificationManager notificationManager) {
        //String channelId = "my_service_channelid";
        String channelName = " ";
        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
    }
    private boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
