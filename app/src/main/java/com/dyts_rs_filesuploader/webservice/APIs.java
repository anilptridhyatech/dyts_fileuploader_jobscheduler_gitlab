package com.dyts_rs_filesuploader.webservice;



public class APIs {
    public static final String PREF_NAME= "my_pref";
    public static final String IS_LOG_IN= "is_log_in";
    public static final String AUTH_TOKEN= "auth_token";

    public static final String BASE_DOMAIN = "https://www.shethsagainfinite.com/public/"; //client server
    public static final String BASE_URL = BASE_DOMAIN + "api/";
    public static final String BASE_IMAGE_PATH = BASE_DOMAIN;
    public static final String BASE_VIDEO_PATH = BASE_DOMAIN;
    public static final String LOCATION_URL = "https://maps.googleapis.com/maps/api/geocode/";

    public static final String LOAD_MORE_LIMIT = "10";

    //Authentication
    public static final String API_LOGIN = "auth/login";

    public static final String API_LOGOUT = "auth/logout";
    public static final String API_FORGOT_PASSWORD = "auth/forgot/password";
    public static final String API_REQUEST_RESET_PASSWORD = "auth/request/reset-password";
    public static final String API_REFRESH_TOKEN = "auth/update/device-token";
    public static final String API_VERIFY_OTP = "auth/login/verify/otp";

    public static final String API_UPLOAD_LOCATION_DATA = "auth/upload/location-data";

    // Get Roaming Staff Configuration
    public static final String API_GET_DRIVER_CONFIGURATION = "get/driver-config";

    // CMS Pages
    public static final String API_GET_CMS = "MasterAPI/GetCMS";

    // User
    public static final String API_GET_PERSONAL_PROFILE = "UserMasterAPI/GetUserMaster";
    public static final String API_GET_CONTACTS = "UserMasterAPI/GetContactList";
    public static final String API_CHANGE_PASSWORD = "auth/change/password";
    public static final String API_EDIT_PERSONAL_PROFILE = "auth/update/profile";

    // Language Selection
    // VEHICLE CHALLANS
    public static final String API_GET_VIOLATION_REASONS = "get/violation-reasons";
    public static final String API_EVENT_DETAILS = "user/details";
    public static final String API_GET_CHALLAN_EXISTS = "challan/exist-or-not";


    public static final String API_GET_MESSAGES = "get/messages";
    public static final String API_GET_MESSAGE_DETAIL = "get/messages-detail";

    //   EVENTS
    public static final String API_GET_REJECTED = "get/rejected-events";
    public static final String API_GET_REJECTED_DETAIL = "get/rejected-event-details";
    public static final String API_UPLOAD_EVENT = "upload/event";
    public static final String API_SET_PREFERRED_LANGUAGE = "setPreferredLanguage";



}