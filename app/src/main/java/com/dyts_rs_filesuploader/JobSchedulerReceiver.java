package com.dyts_rs_filesuploader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;


public class JobSchedulerReceiver extends BroadcastReceiver {
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        try {
            Intent intent2 = new Intent(context, JobForegroundService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent2);

            } else {
                context.startService(new Intent(intent2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}